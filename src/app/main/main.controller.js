(function() {
  'use strict';

  angular
    .module('balesteriConst')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, recentProjects) {
    var vm = this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.showToastr = showToastr;

    activate();

    function activate() {
      getWebDevTec();
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = recentProjects.getProject();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }
  }
})();

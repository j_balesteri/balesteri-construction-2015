(function() {
  'use strict';

  angular
    .module('balesteriConst', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ngMaterial']);

})();

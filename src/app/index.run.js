(function() {
  'use strict';

  angular
    .module('balesteriConst')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();

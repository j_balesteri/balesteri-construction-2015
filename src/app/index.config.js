(function() {
  'use strict';

  angular
    .module('balesteriConst')
    .config(theming)
    .config(config);

  /** @ngInject */
  function config($logProvider, toastr) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastr.options.timeOut = 3000;
    toastr.options.positionClass = 'toast-top-right';
    toastr.options.preventDuplicates = true;
    toastr.options.progressBar = true;
  }

  function theming($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('blue', {
            'default': '900',
            'hue-1': '100'
        })
        .accentPalette('pink',{
            'default': '700',
            'hue-1': '50'
        });
}

})();

(function() {
  'use strict';

  angular
      .module('balesteriConst')
      .service('recentProjects', recentProjects);

  /** @ngInject */
  function recentProjects() {
    var data = [
      {
        'title': 'Project One',
        // 'url': 'https://angularjs.org/',
        'description': 'Lorem ipsum Duis amet laborum ad fugiat cupidatat Duis cupidatat occaecat in elit non fugiat mollit qui nisi veniam occaecat do ut irure sunt consequat.',
        'logo': 'house3thumb.jpg'
      },
      {
        'title': 'Project Two',
        // 'url': 'http://browsersync.io/',
        'description': 'Lorem ipsum Velit laborum pariatur dolor dolor et velit cillum anim laboris sunt amet laborum quis mollit veniam mollit labore qui laboris commodo et consectetur est ad in ut Duis laboris occaecat in veniam adipisicing velit proident in est minim commodo do.',
        'logo': 'house2thumb.jpg'
      },
      {
        'title': 'Project Three',
        // 'url': 'http://gulpjs.com/',
        'description': 'Lorem ipsum Esse aute in ullamco eiusmod exercitation Ut fugiat sunt elit in exercitation commodo culpa tempor do exercitation ea sed adipisicing mollit aute eiusmod nulla dolor aliqua aliquip non.',
        'logo': 'house4thumb.jpg'
      },


    ];

    this.getProject = getProject;

    function getProject() {
      return data;
    }
  }

})();

(function() {
  'use strict';

  angular
    .module('balesteriConst')
    .directive('acmeMalarkey', acmeMalarkey);

  /** @ngInject */
  function acmeMalarkey(malarkey) {
    var directive = {
      restrict: 'E',
      scope: {
        extraValues: '=',
      },
      template: '&nbsp;',
      link: linkFunc,
      controller: MalarkeyController,
      controllerAs: 'vm'
    };

    return directive;

    function linkFunc(scope, el) {
      var typist = malarkey(el[0], {
        typeSpeed: 70,
        deleteSpeed: 30,
        pauseDelay: 900,
        loop: true,
        postfix: ' '
      });

      el.addClass('acme-malarkey');

      angular.forEach(scope.extraValues, function(value) {
        typist.type(value).pause().delete();
      });
    }

    /** @ngInject */
    function MalarkeyController() {

    }
  }
})();
